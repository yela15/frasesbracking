import React, { useState, useEffect } from 'react';
import Frase from './components/Frase'
import {numeroAleatorio} from './helper'
import styled from '@emotion/styled';

const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
`;

const Boton = styled.button`
  background: -webkit-linear-gradient(top left, #007d35, #007d35 40%, #0f574e 100%);
  background-size: 300px;
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 2rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
  transition: background-size .8s ease;

  :hover {
    cursor: pointer;
    background-size: 400px;
  }
`; 

/** uso de FETH
 * const consultarAPI = () => {
  const api = fetch('https://breakingbadapi.com/api/quotes/');
  const frase = api.then(respuesta => respuesta.json());
  frase.then(resultado => console.log(resultado));
 }

 * // uso de asing await
  const consultarAPI = async() => {
  const api = await fetch('https://breakingbadapi.com/api/quotes/');
  const frase = await api.json()
  guardarFrase(frase[0]);
}
 */

/**
 * diferencias de consulta 
 * {() => consultarAPI()} >>> espera q que el usuario de click
 * {consultarAPI()} >>> ejecutara direcctamente
 * {consultarAPI} >>> cuando preciona de recien ejecuta
 */

function App() {

  // state de frases
  const [frase, guardarFrase] = useState({});

  // numero aleatorio entre 0 y 69
  let numeroAl = numeroAleatorio(0,69)

   // uso de asing await
  const consultarAPI = async() => {
    const api = await fetch('https://breakingbadapi.com/api/quotes/');
    const frase = await api.json()
    guardarFrase(frase[numeroAl]);
    
  }

  // de forma automatica
  //cargar una frase ( cunado el componente carga consulta a la api)
  useEffect(() => {
    consultarAPI();
  }, []);

  return (
    <Contenedor>
      <Frase
        frase = {frase}
      />
      <Boton
        onClick={consultarAPI}
      >
        Obtener Frase
      </Boton>
    </Contenedor>
  );
}

export default App;

// aniadiendo librerias
// npm i @emotion/core @emotion/styled